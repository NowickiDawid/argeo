package pl.wat.wcy.tim.argeo;

import pl.wat.wcy.tim.argeo.service.SensorService;

import com.qualcomm.QCARUnityPlayer.QCARPlayerSharedActivity;
import com.unity3d.player.*;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import android.widget.LinearLayout;

public class ArGeoActivity extends Activity {
	private ArGeoControl arGeoControl;
	private QCARPlayerSharedActivity qCARShared;
	private UnityPlayer unityPlayer;

	private TextView textGPS;
	private ARGeoReceiver receiver;

	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		unityPlayer = new UnityPlayer(this);
		qCARShared = new QCARPlayerSharedActivity();
		int gles_mode = unityPlayer.getSettings().getInt("gles_mode", 1);
		qCARShared.onCreate(this, gles_mode, new UnityInitializer());
		getWindow().takeSurface(null);
		setTheme(android.R.style.Theme_NoTitleBar_Fullscreen);
		getWindow().setFormat(PixelFormat.RGB_565);

		if (unityPlayer.getSettings().getBoolean("hide_status_bar", true))
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.main_layout);

		receiver = new ARGeoReceiver();
	}

	protected void onDestroy() {
		unityPlayer.quit();
		super.onDestroy();
	}

	public class ARGeoReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			int[] values = intent.getIntArrayExtra(SensorService.ROTATION_KEYS);
			if (values != null && arGeoControl != null) {

				if (values[0] > -90 && values[0] < 180)
					values[0] += 90;
				else {
					values[0] = 270 + (180 + values[0]);
				}

				if (values[2] > -90 && values[2] < 180)
					values[2] += 90;
				else {
					values[2] = 270 + (180 + values[2]);
				}

				Log.i("rootate", -values[0] + " " + values[1] + " " + values[2]);

				arGeoControl.rootate(0, -values[0], 0, -values[2], 0, 0);
			}
		}

	}

	protected void onPause() {
		super.onPause();
		unityPlayer.pause();
		unregisterReceiver(receiver);
		unbindService();

	}

	protected void onResume() {
		super.onResume();
		unityPlayer.resume();

		registerReceiver(receiver, new IntentFilter(SensorService.SENSOR_ACTION));
		bindService();
	}

	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		unityPlayer.configurationChanged(newConfig);
	}

	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		unityPlayer.windowFocusChanged(hasFocus);
	}

	public boolean dispatchKeyEvent(KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
			return unityPlayer.onKeyMultiple(event.getKeyCode(), event.getRepeatCount(), event);
		return super.dispatchKeyEvent(event);
	}

	private class UnityInitializer implements QCARPlayerSharedActivity.IUnityInitializer {
		public void InitializeUnity() {
			int glesMode = unityPlayer.getSettings().getInt("gles_mode", 1);
			boolean trueColor8888 = false;
			unityPlayer.init(glesMode, trueColor8888);

			View playerView = unityPlayer.getView();

			arGeoControl = new ArGeoControl(ArGeoActivity.this);
			textGPS = ((TextView) findViewById(R.id.azimuthValue));
			((LinearLayout) findViewById(R.id.unityLayout)).addView(playerView);
			((ImageButton) findViewById(R.id.imageButton1)).setOnClickListener(new OnClickListener() {
				private boolean serializing = false;

				@Override
				public synchronized void onClick(View v) {
					service.setSerializing(!serializing);
					serializing = !serializing;
				}
			});
			arGeoControl.setPostion(52.234232f, 62.23242f);
		}
	}

	public TextView getGPS() {
		return textGPS;
	}

	private void bindService() {
		Intent intent = new Intent(this, SensorService.class);
		bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
	}

	private void unbindService() {
		if (isRegistered) {
			unbindService(serviceConnection);
			isRegistered = false;
		}
	}

	private SensorService service;
	private boolean isRegistered = false;

	private ServiceConnection serviceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder ibinder) {
			SensorService.SensorServiceBinder binder = (SensorService.SensorServiceBinder) ibinder;
			service = binder.getService();
			service.setSerializing(false);
			isRegistered = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
		}
	};

}
