package pl.wat.wcy.tim.argeo;

import android.util.Log;
import android.widget.TextView;

import com.qualcomm.vuforia.TextTracker;
import com.unity3d.player.UnityPlayer;

public class ArGeoControl {

	ArGeoActivity arGeoActivity;
	private Thread mRot;

	public ArGeoControl(ArGeoActivity arGeoActivity) {
		super();
		this.arGeoActivity = arGeoActivity;

	}

	public void setPostion(float lat, float lng) {
		TextView t = arGeoActivity.getGPS();
		t.setText(arGeoActivity.getString(R.string.GPS_cord, lat, lng));
	}

	public void rootate(float x, float y, float z, float x1, float y1, float z1) {

		
		StringBuilder stream = new StringBuilder();
		stream.append(x);
		stream.append(",");
		stream.append(y);
		stream.append(",");
		stream.append(z);
		stream.append(",");
		stream.append(x1);
		stream.append(",");
		stream.append(y1);
		stream.append(",");
		stream.append(z1);
		UnityPlayer.UnitySendMessage("ARCamera", "rotate", stream.toString());
	}

	


	public void saveButtonClicked() {

		Log.i("saveButtonClicked", "Click");

	}

	public void changeValues(int[] values) {
		// TODO Auto-generated method stub
		
	}

}
