package pl.wat.wcy.tim.argeo;

import pl.wat.wcy.tim.argeo.service.SensorService;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * demo
 * 
 * @author Micha�
 * 
 */
public class ServiceTest extends Activity implements OnClickListener {

	private Button gogogo;
	private Button stop;
	private TextView azimuthValue;

	private MyReceiver receiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_service_test);
		gogogo = (Button) findViewById(R.id.gogogo);
		gogogo.setOnClickListener(this);

		stop = (Button) findViewById(R.id.stop);
		stop.setOnClickListener(this);

		azimuthValue = (TextView) findViewById(R.id.azimuthValue);

		receiver = new MyReceiver();

	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(this, SensorService.class);
		if (v.equals(gogogo)) {
			startService(intent);
			bindService();
		} else {
			stopService(intent);
			unbindService();
		}
	}

	public class MyReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			int[] values = intent.getIntArrayExtra(SensorService.ROTATION_KEYS);
			if (values != null) {
				azimuthValue.setText("azimuth: " + values[0] + " pitch: " + values[1] + " roll: " + values[2]);
			}
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(receiver, new IntentFilter(SensorService.SENSOR_ACTION));
	}

	@Override
	protected void onPause() {
		unregisterReceiver(receiver);
		unbindService();
		super.onPause();
	}

	/**
	 * u�yj aby zbindowa� us�uge
	 */
	private void bindService() {
		Intent intent = new Intent(this, SensorService.class);
		bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
	}

	private void unbindService() {
		if (isRegistered) {
			unbindService(serviceConnection);
			isRegistered = false;
		}
	}

	/**
	 * zbinduj aby mie� dost�p do us�ugi
	 */
	private SensorService service;
	private boolean isRegistered = false;

	private ServiceConnection serviceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder ibinder) {
			SensorService.SensorServiceBinder binder = (SensorService.SensorServiceBinder) ibinder;
			service = binder.getService();
			/**
			 * czy zapisujemy
			 */
			service.setSerializing(false);
			isRegistered = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
		}
	};

}
