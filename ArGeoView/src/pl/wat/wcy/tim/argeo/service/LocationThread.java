package pl.wat.wcy.tim.argeo.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

public class LocationThread extends Thread implements LocationListener{
	private static String extStore = Environment.getExternalStorageDirectory().getAbsolutePath() + "/tim-gps.txt";
	
	private static final int SAVE_DELAY = 5*1000;
	
	private List<Location> locations = new LinkedList<Location>();
	
	private boolean running = true;

	@Override
	public void onLocationChanged(Location location) {
		synchronized (locations) {
			locations.add(location);
		}		
	}

	@Override
	public void onProviderDisabled(String provider) {
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}
	
	public void stopIt(){
		running = false;
	}

	@Override
	public void run() {
		while(running){			
			waitDelayTime();
			save();
		}
	}

	private void save() {
		synchronized(locations){
			PrintWriter out = null;
			try {
				out = new PrintWriter(new BufferedWriter(new FileWriter(extStore, true)));
				for (Location location : locations) {
					out.println(serializeLocation(location));
				}
				locations.clear();
			} catch (IOException e) {
				Log.d(LocationThread.class.getSimpleName(), e.getMessage());
			} finally {
				if (out != null) {
					out.close();
				}
			}
		}
	}

	private String serializeLocation(Location location) {
		return "Altitude: " + location.getAltitude() + " latitude: " + location.getLatitude() + " accuaracy: " + location.getAccuracy();
	}

	private void waitDelayTime() {
		synchronized(this){
			try {
				wait(SAVE_DELAY);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
