package pl.wat.wcy.tim.argeo.service;

import android.hardware.SensorEvent;
import android.hardware.SensorManager;

public class OrientationCalculator {	
	private SensorValues sensValues = new SensorValues();
	private float[] orientation = new float[] { 0.0f, 0.0f, 0.0f };

	public float[] calculateCompass() {
		float R[] = new float[9];
		if (getRotationMatrixR(R)) {
			orientation = new float[3];
			SensorManager.getOrientation(R, orientation);
		}
		return orientation;
	}
	
	public Integer[] calculateInDegrees(){
		calculateCompass();
		Integer degrees[] = new Integer[3];
		for (int i = 0; i < degrees.length; i++) {
			degrees[i] = (int) toDegrees(orientation[i]);
		}

		return degrees;
	}

	private double toDegrees(float azimuth) {
		return azimuth * (180 / Math.PI);
	}

	private boolean getRotationMatrixR(float[] R) {
		return SensorManager.getRotationMatrix(R, null, sensValues.accelometerValues, sensValues.magneticValues);
	}

	public void onMagneticChange(SensorEvent event) {
		sensValues.magneticValues = event.values.clone();
		sensValues.magneticTimestamp = event.timestamp;
	}

	public void onAccelometerChanged(SensorEvent event) {
		sensValues.accelometerValues = event.values.clone();
		sensValues.accelometerTimestamp = event.timestamp;
	}

	public boolean isAnyValuesRegistered() {
		return sensValues.accelometerTimestamp > 0 && sensValues.magneticTimestamp > 0;
	}
}
