package pl.wat.wcy.tim.argeo.service;

import java.util.LinkedList;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.os.Binder;
import android.os.IBinder;

public class SensorService extends Service implements SensorEventListener {
	public static final String SENSOR_ACTION = SensorService.class.getName() + ".SENSOR_ACTION";
	public static final String ROTATION_KEYS = SensorService.class.getName() + ".ROTATION";
	public static final String TAG = SensorService.class.getSimpleName();

	public class SensorServiceBinder extends Binder {
		public SensorService getService() {
			return SensorService.this;
		}
	}

	private LinkedList<Integer[]> values = new LinkedList<Integer[]>();

	private Binder binder = new SensorServiceBinder();

	private LocationManager locationManager;

	private SensorManager sensorManager;
	private Sensor accelometer;
	private Sensor magnetic;

	private LocationThread locationThread;

	private OrientationCalculator orientation = new OrientationCalculator();

	@Override
	public IBinder onBind(Intent arg0) {
		return binder;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		initSensors();
		registerSensorListeners();
	}

	@Override
	public void onDestroy() {
		sensorManager.unregisterListener(this);
		super.onDestroy();
	}

	private void initSensors() {
		locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		accelometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		magnetic = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
	}

	private void registerSensorListeners() {
		sensorManager.registerListener(this, accelometer, SensorManager.SENSOR_DELAY_NORMAL);
		sensorManager.registerListener(this, magnetic, SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		onSensorEvent(event);
		tryCalculateCompass();
	}

	private synchronized void tryCalculateCompass() {
		if (orientation.isAnyValuesRegistered()) {
			handleNewValue();
		}
	}

	private void handleNewValue() {
		Integer[] orientations =  orientation.calculateInDegrees();
		sendNewValuesBroadcast(orientations);
		values.addLast(orientations);
	}

	private void sendNewValuesBroadcast(Integer[] vals) {
		int[] values = SensorUtils.toPrimitiveArray(vals);
		Intent intnet = new Intent(SENSOR_ACTION);
		intnet.putExtra(ROTATION_KEYS, values);
		sendBroadcast(intnet);
	}

	private synchronized void onSensorEvent(SensorEvent event) {
		if (SensorUtils.isAccelmeter(event)) {
			orientation.onAccelometerChanged(event);
		} else if (SensorUtils.isMagnetic(event)) {
			orientation.onMagneticChange(event);
		}
	}

	public void setSerializing(boolean serialize) {
		if (serialize == true) {
			locationThread = new LocationThread();
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, locationThread);
			locationThread.start();
		} else if (locationThread != null) {
			locationManager.removeUpdates(locationThread);
			locationThread.stopIt();
		}
	}

}
