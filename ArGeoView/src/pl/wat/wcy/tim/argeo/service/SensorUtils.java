package pl.wat.wcy.tim.argeo.service;


import android.hardware.Sensor;
import android.hardware.SensorEvent;

public class SensorUtils {
	public static boolean isMagnetic(SensorEvent event){
		return event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD;
	}
	
	public static boolean isAccelmeter(SensorEvent event){
		return event.sensor.getType() == Sensor.TYPE_ACCELEROMETER;
	}
	
	
	
	public static int[] toPrimitiveArray(Integer[] vals) {
		int values[] = new int[vals.length];
		for (int i = 0; i < vals.length; i++) {
			values[i] = vals[i];
		}
		return values;
	}
}
